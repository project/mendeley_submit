<?php

/**
 * @file
 * Web service for Mendeley.
 *
 * Date: 26 Nov 2019 18:10:07
 * File: mendeley_submit.api.php
 * Author: stefano.
 */

/**
 * Core document attributes.
 *
 * SC 03 Dec 2019 15:09:03 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of element:
 *   Attribute      Type    Max len   Description
 *   id             string            Identifier (UUID) of the document. This identifier is set by the server on create and it cannot be modified. You need it only when query, update or delete the document.
 *   title          string  255       Title of the document. This is a required field.
 *   type           string            The type of the document. Supported types: journal, book, generic, book_section, conference_proceedings, working_paper, report, web_page, thesis, magazine_article, statute, patent, newspaper_article, computer_program, hearing, television_broadcast, encyclopedia_article, case, film, bill.
 *   profile_id     string            Profile id (UUID) of the Mendeley user that added the document to the system.
 *   group_id       string            Group id (UUID) that the document belongs to.
 *   created        string            Date the document was added to the system. This date is set by the server after a successful create request. This date is represented in ISO 8601 format.
 *   last_modified  string            Date in which the document was last modified. This date is set by the server after a successful update request. This date is represented in ISO 8601 format.
 *   abstract       string  10000     Brief summary of the document.
 *   source         string  255       Publication outlet, i.e. where the document was published.
 *   year           int               Year in which the document was issued/published.
 *   identifiers    array   500 each  List of identifiers available for the document. The supported identifiers are: arxiv, doi, isbn, issn, pmid (PubMed), scopus and ssrn.
 *   keywords       array   50 each   List of author-supplied keywords for the document.
 */
function hook_mendeley_submit_document_core($node) {
  return [
    'mendeley_submit_document_core' => [
      'id' => '',
      'title' => '',
      'type' => '',
      'profile_id' => '',
      'group_id' => '',
      'created' => '',
      'last_modified' => '',
      'abstract' => '',
      'source' => '',
      'year' => 2019,
      'identifiers' => [],
      'keywords' => [],
    ],
  ];
}

/**
 * Additional document attributes.
 *
 * The following attributes are available for use with a view.
 *
 * SC 03 Dec 2019 15:29:17 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of element:
 *   Attribute                  Type    Max len  Description
 *   month                      int              Month in which the document was issued/published. This should be an integer between 1 and 12.
 *   day                        int              Day in which the document was issued/published. This should be an integer between 1 and 31.
 *   revision                   string  255      Number identifying the item (e.g. a report number).
 *   pages                      string  50       Range of pages the document covers in the issue, e.g 4-8.
 *   volume                     string  10       Volume holding the document, e.g “2” from journal volume 2.
 *   issue                      string  255      Issue holding the document, e.g. “5” for a journal article from journal volume 2, issue 5
 *   websites                   array   255 tot  Where the document was accessed from.
 *   publisher                  string  255      Publisher of the document.
 *   city                       string  255      City where the document was published.
 *   edition                    array   10       Edition holding the document, e.g. “3” from the third edition of a book.
 *   institution                string  255      Institution in which the document was published.
 *   series                     string  255      Title of the collection holding the document (e.g. the series title for a book)
 *   chapter                    string  10       Chapter number.
 *   accessed                   string           Date that the document was accessed. This date is represented in ISO 8601 format.
 *   tags                       array   50 each  List of user-supplied tags for the document.
 *   read                       bool             Flag used to identify whether the document has been read or not.
 *   starred                    bool             Flag used to identify whether the user has marked the document as favourite.
 *   authored                   bool             Flag used to identify whether the user has authored the document.
 *   confirmed                  bool             Flag to identify whether the metadata of the document is correct after it has been extracted from the PDF file.
 *   hidden                     bool             Flag to identify whether Mendeley can publish this document to the Mendeley catalog.
 *   file_attached              bool             Flag to identify whether the document has a file attached. Files can be retrieved using the files API.
 *   citation_key               string  255      Identifier used by BibTeX when citing documents.
 *   source_type                string  255      Original type of a document imported from BibTeX.
 *   language                   string  255      Language of the document.
 *   short_title                string  50       A short version of the title, used in some citations.
 *   reprint_edition            string  10       Edition number of this reprint of the document.
 *   genre                      string  255      Class, type or genre of the document.
 *   country                    string  255      Country where the document was published.
 *   series_editor              string  255      Editor name for a book series.
 *   code                       string  255      Code for legal documents.
 *   medium                     string  255      Medium description (e.g. “CD”, “DVD”).
 *   user_context               string  255      The format of the item (e.g. Journal)
 *   department                 string  255      University department name.
 *   patent_owner               string  255      Name of patent assignee.
 *   patent_application_number  string  255      Patent application number.
 *   patent_legal_status        string  255      Legal status of document, example values “Restricted”, “Pending approval”, “Granted”.
 */
function hook_mendeley_submit_document_extra($node) {
  return [
    'mendeley_submit_document_extra' => [
      'month' => 1,
      'day' => 1,
      'revision' => '',
      'pages' => '',
      'volume' => '',
      'issue' => '',
      'websites' => [],
      'publisher' => '',
      'city' => '',
      'edition' => [],
      'institution' => '',
      'series' => '',
      'chapter' => '',
      'accessed' => '',
      'tags' => [],
      'read' => FALSE,
      'starred' => FALSE,
      'authored' => FALSE,
      'confirmed' => FALSE,
      'hidden' => FALSE,
      'file_attached' => FALSE,
      'citation_key' => '',
      'source_type' => '',
      'language' => '',
      'short_title' => '',
      'reprint_edition' => '',
      'genre' => '',
      'country' => '',
      'series_editor' => '',
      'code' => '',
      'medium' => '',
      'user_context' => '',
      'department' => '',
      'patent_owner' => '',
      'patent_application_number' => '',
      'patent_legal_status' => '',
    ],
  ];
}

/**
 * Authors.
 *
 * People are represented as an array with the following attributes.
 *
 * SC 03 Dec 2019 16:02:25 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of array of elements:
 *   Attribute   Type    Max len  Description
 *   last_name   string  255      Last name of the person. Required.
 *   first_name  string  255      First name of the person.
 */
function hook_mendeley_submit_document_authors($node) {
  return [
    'mendeley_submit_document_authors' => [
      [
        'last_name' => 'Primo',
        'first_name' => 'Uno',
      ],
      [
        'last_name' => 'Secondo',
        'first_name' => 'Due',
      ],
    ],
  ];
}

/**
 * Editors.
 *
 * People are represented as an array with the following attributes.
 *
 * SC 03 Dec 2019 16:02:25 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of array of elements:
 *   Attribute   Type    Max len  Description
 *   last_name   string  255      Last name of the person. Required.
 *   first_name  string  255      First name of the person.
 */
function hook_mendeley_submit_document_editors($node) {
  return [
    'mendeley_submit_document_editors' => [
      [
        'last_name' => 'Primo',
        'first_name' => 'Uno',
      ],
      [
        'last_name' => 'Secondo',
        'first_name' => 'Due',
      ],
    ],
  ];
}

/**
 * Translators.
 *
 * People are represented as an array with the following attributes.
 *
 * SC 03 Dec 2019 16:02:25 stefano.
 *
 * @param object $node
 *   The node object.
 *
 * @return array
 *   An array of array of elements:
 *   Attribute   Type    Max len  Description
 *   last_name   string  255      Last name of the person. Required.
 *   first_name  string  255      First name of the person.
 */
function hook_mendeley_submit_document_translators($node) {
  return [
    'mendeley_submit_document_translators' => [
      [
        'last_name' => 'Primo',
        'first_name' => 'Uno',
      ],
      [
        'last_name' => 'Secondo',
        'first_name' => 'Due',
      ],
    ],
  ];
}
