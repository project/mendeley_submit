<?php

/**
 * @file
 * Web service for Mendeley.
 *
 * Class to interface with the web service.
 *
 * @brief A wrapper for the Mendeley web service.
 *
 *
 * Date: 26 Nov 2019 16:43:00
 * File: mendeley_submit.class.inc
 * Author: stefano.
 */

/**
 * Web Service interface.
 *
 * SC 26 Nov 2019 16:44:58 stefano.
 */
class MendeleySubmit {

  /**
   * The token for authentication.
   *
   * @var string
   *
   * SC 28 Nov 2019 10:16:24 stefano.
   */
  private $token;

  /**
   * The endpoints that the API offers.
   *
   * @var string
   *
   * SC 28 Nov 2019 12:06:19 stefano.
   */
  private $endpoint;

  /**
   * CURL handle.
   *
   * @var resource
   *
   * SC 28 Nov 2019 10:50:20 stefano.
   */
  private $ch;

  /**
   * Headers.
   *
   * @var array
   *
   * SC 28 Nov 2019 11:36:33 stefano.
   */
  private $extraHeaders;

  /**
   * The result from curl_exec().
   *
   * @var mixed
   *
   * SC 09 Jan 2020 16:04:47 stefano.
   */
  private $result;

  /**
   * Construct.
   *
   * SC 28 Nov 2019 10:23:28 stefano.
   */
  public function __construct() {

    if (variable_get('mendeley_submit_endpoint', '') == 'sandbox') {
      $this->token = variable_get('mendeley_submit_token_s', '');
      $this->endpoint = MENDELEY_SUBMIT_ENDPOINT_DEV;
    }
    else {
      try {
        $oauth2_client = oauth2_client_load('user');
        $this->token = $oauth2_client->getAccessToken();
      }
      catch (Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
      }

      $this->endpoint = MENDELEY_SUBMIT_ENDPOINT;
    }

    // Initialize curl.
    //
    // SC 28 Nov 2019 10:59:26 stefano.
    //
    $this->ch = curl_init();
    curl_setopt($this->ch, CURLOPT_VERBOSE, TRUE);
    curl_setopt($this->ch, CURLOPT_HEADER, TRUE);
    curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);

    $this->addHeader('Authorization: Bearer ' . $this->token);
  }

  /**
   * Execute.
   *
   * SC 28 Nov 2019 11:25:33 stefano.
   */
  public function send() {

    curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->extraHeaders);

    $this->result = curl_exec($this->ch);
  }

  /**
   * Set headers.
   *
   * SC 28 Nov 2019 11:30:38 stefano.
   *
   * @param string $header
   *   The values to add.
   */
  private function addHeader($header) {
    $this->extraHeaders[] = $header;
  }

}

/**
 * Documents Mendeley schema.
 *
 * SC 26 Nov 2019 18:34:05 stefano.
 */
class MendeleySubmitDocumentsDataSchema {

  /**
   * Identifier (UUID) of the document.
   *
   * This identifier is set by the server on create and it cannot be modified.
   *
   * @var string
   *
   * SC 26 Nov 2019 18:13:52 stefano.
   */
  private $id;

  /**
   * Title of the document.
   *
   * This is a required field.
   *
   * Max lenght 255 chars.
   *
   * @var string
   *
   * SC 26 Nov 2019 18:15:55 stefano.
   */
  private $title;

  /**
   * The type of the document.
   *
   * Supported types: journal, book, generic, book_section,
   * conference_proceedings, working_paper, report, web_page, thesis,
   * magazine_article, statute, patent, newspaper_article, computer_program,
   * hearing, television_broadcast, encyclopedia_article, case, film, bill.
   *
   * @var string
   *
   * SC 26 Nov 2019 18:17:05 stefano.
   */
  private $type;

  /**
   * Profile id.
   *
   * The UUID of the Mendeley user that added the document to the system.
   *
   * @var string
   *
   * SC 26 Nov 2019 18:21:50 stefano.
   */
  private $profile_id;

  /**
   * Group id.
   *
   * The UUID that the document belongs to.
   *
   * @var string
   *
   * SC 26 Nov 2019 18:22:49 stefano.
   */
  private $group_id;

  /**
   * Date the document was added to the system.
   *
   * This date is set by the server after a successful create request. This
   * date is represented in ISO 8601 format.
   *
   * @var string
   *
   * SC 26 Nov 2019 18:24:00 stefano.
   */
  private $created;

  /**
   * Date in which the document was last modified.
   *
   * This date is set by the server after a successful update request. This
   * date is represented in ISO 8601 format .
   *
   * @var string
   *
   * SC 26 Nov 2019 18:25:45 stefano.
   */
  private $last_modified;

  /**
   * Brief summary of the document.
   *
   * Max length 10000 chars.
   *
   * @var string
   *
   * SC 26 Nov 2019 18:27:25 stefano.
   */
  private $abstract;

  /**
   * Publication outlet, i.e. where the document was published.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 26 Nov 2019 18:31:28 stefano.
   */
  private $source;

  /**
   * Year in which the document was issued / published.
   *
   * @var int
   *
   * SC 26 Nov 2019 18:35:17 stefano.
   */
  private $year;

  /**
   * List of authors of the document, as person objects.
   *
   * @var array
   *
   * SC 26 Nov 2019 18:36:16 stefano.
   */
  private $authors;

  /**
   * List of identifiers available for the document.
   *
   * The supported identifiers are: arxiv, doi, isbn, issn, pmid(PubMed),
   * scopus and ssrn .
   *
   * Max length 500 each.
   *
   * @var array
   *
   * SC 26 Nov 2019 18:39:05 stefano.
   */
  private $identifiers;

  /**
   * List of author - supplied keywords for the document.
   *
   * Max length 50 each.
   *
   * @var array
   * SC 26 Nov 2019 18:40:49 stefano.
   */
  private $keywords;

  // Additional document attributes.
  //
  // The following attributes are available using a view.
  //
  // SC 02 Dec 2019 11:38:50 stefano.
  //
  /**
   * Month in which the document was issued/published.
   *
   * This should be an integer between 1 and 12.
   *
   * @var int
   *
   * SC 02 Dec 2019 12:49:15 stefano.
   */
  private $month;

  /**
   * Day in which the document was issued/published.
   *
   * This should be an integer between 1 and 31.
   *
   * @var int
   *
   * SC 02 Dec 2019 12:50:19 stefano.
   */
  private $day;

  /**
   * Number identifying the item (e.g. a report number).
   *
   * Max length 255 chars.
   *
   * @var string
   * SC 02 Dec 2019 12:51:16 stefano.
   */
  private $revision;

  /**
   * Range of pages the document covers in the issue, e.g 4-8.
   *
   * Max length 50 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 12:52:35 stefano.
   */
  private $pages;

  /**
   * Volume holding the document, e.g “2” from journal volume 2.
   *
   * Max length 10 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 12:53:52 stefano.
   */
  private $volume;

  /**
   * Issue holding the document, e.g. “5” for a journal article from journal volume 2, issue 5.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 12:54:52 stefano.
   */
  private $issue;

  /**
   * Where the document was accessed from.
   *
   * Max 255 total.
   *
   * @var array
   * SC 02 Dec 2019 16:20:09 stefano.
   */
  private $websites;

  /**
   * Publisher of the document.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:21:32 stefano.
   */
  private $publisher;

  /**
   * City where the document was published.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:22:08 stefano.
   */
  private $city;

  /**
   * Edition holding the document, e.g. “3” from the third edition of a book.
   *
   * Max 10.
   *
   * @var array
   *
   * SC 02 Dec 2019 16:22:50 stefano.
   */
  private $edition;

  /**
   * Institution in which the document was published.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:23:45 stefano.
   */
  private $institution;

  /**
   * Title of the collection holding the document, e.g. the series title for a book.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:24:47 stefano.
   */
  private $series;

  /**
   * Chapter number.
   *
   * Max length 10 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:25:31 stefano.
   */
  private $chapter;

  /**
   * List of editors of the document, as person objects.
   *
   * @var array
   *
   * SC 02 Dec 2019 16:26:06 stefano.
   */
  private $editors;

  /**
   * Date that the document was accessed.
   *
   * This date is represented in ISO 8601 format.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:27:53 stefano.
   */
  private $accessed;

  /**
   * List of user-supplied tags for the document.
   *
   * Max length 50 each;
   *
   * @var array
   *
   * SC 02 Dec 2019 16:28:28 stefano.
   */
  private $tags;

  /**
   * Flag used to identify whether the document has been read or not.
   *
   * @var bool
   *
   * SC 02 Dec 2019 16:30:00 stefano.
   */
  private $read;

  /**
   * Flag used to identify whether the user has marked the document as favourite.
   *
   * @var bool
   *
   * SC 02 Dec 2019 16:31:00 stefano.
   */
  private $starred;

  /**
   * Flag used to identify whether the user has authored the document.
   *
   * @var bool
   *
   * SC 02 Dec 2019 16:31:36 stefano.
   */
  private $authored;

  /**
   * Flag to identify whether the metadata of the document is correct after it has been extracted from the PDF file.
   *
   * @var bool
   *
   * SC 02 Dec 2019 16:32:01 stefano.
   */
  private $confirmed;

  /**
   * Flag to identify whether Mendeley can publish this document to the Mendeley catalog.
   *
   * @var bool
   *
   * SC 02 Dec 2019 16:32:57 stefano.
   */
  private $hidden;

  /**
   * Flag to identify whether the document has a file attached.
   *
   * Files can be retrieved using the files API.
   *
   * @var bool
   *
   * SC 02 Dec 2019 16:34:55 stefano.
   */
  private $file_attached;

  /**
   * Identifier used by BibTeX when citing documents.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:35:44 stefano.
   */
  private $citation_key;

  /**
   * Original type of a document imported from BibTeX.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:37:10 stefano.
   */
  private $source_type;

  /**
   * Language of the document.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:37:44 stefano.
   */
  private $language;

  /**
   * A short version of the title, used in some citations.
   *
   * Max length 50 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:38:29 stefano.
   */
  private $short_title;

  /**
   * Edition number of this reprint of the document.
   *
   * Max length 10 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:39:10 stefano.
   */
  private $reprint_edition;

  /**
   * Class, type or genre of the document.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:39:35 stefano.
   */
  private $genre;

  /**
   * Country where the document was published.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:40:09 stefano.
   */
  private $country;

  /**
   * List of translators of the document, as person objects.
   *
   * @var array
   *
   * SC 02 Dec 2019 16:40:46 stefano.
   */
  private $translators;

  /**
   * Editor name for a book series.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:41:18 stefano.
   */
  private $series_editor;

  /**
   * Code for legal documents.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:41:47 stefano.
   */
  private $code;

  /**
   * Medium description (e.g. “CD”, “DVD”).
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:42:16 stefano.
   */
  private $medium;

  /**
   * The format of the item (e.g. Journal).
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:42:38 stefano.
   */
  private $user_context;

  /**
   * University department name.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:43:54 stefano.
   */
  private $department;

  /**
   * Name of patent assignee.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:44:14 stefano.
   */
  private $patent_owner;

  /**
   * Patent application number.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:44:42 stefano.
   */
  private $patent_application_number;

  /**
   * Legal status of document.
   *
   * Example values “Restricted”, “Pending approval”, “Granted”.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 02 Dec 2019 16:45:11 stefano.
   */
  private $patent_legal_status;

}

/**
 * Documents data.
 *
 * SC 26 Nov 2019 18:34:05 stefano.
 */
class MendeleySubmitDocumentsData {

  /**
   * Data for json encode.
   *
   * @var array
   *
   * SC 09 Jan 2020 11:01:38 stefano.
   */
  private $jsonSource;

  /**
   * Construct.
   *
   * SC 03 Dec 2019 17:01:11 stefano.
   *
   * @param int $nid
   *   The node id.
   */
  public function __construct($nid) {

    $this->jsonSource = new MendeleySubmitDocumentsDataSchema();

    list($node, $node_i18n) = $this->getRealNode($nid);

    if (is_object($node) && $node->status) {

      $documentsCore = module_invoke_all('mendeley_submit_document_core', $node);

      if (!empty($documentsCore)) {

        $required = [
          'title' => '',
        ];

        $foo_mendeley_submit_document_core = array_merge($required, $documentsCore['mendeley_submit_document_core']);

        foreach ($foo_mendeley_submit_document_core as $key => $value) {
          switch ($key) {
            case 'title':
              if (empty($value)) {
                drupal_set_message(t('@key is required.', ['@key' => $key]), 'error', FALSE);
              }
              else {
                $this->jsonSource->$key = $this->setMaxString($key, $value, 255);
              }
              break;

            case 'source':
              $this->jsonSource->$key = $this->setMaxString($key, $value, 255);
              break;

            case 'abstract':
              $this->jsonSource->$key = $this->setMaxString($key, $value, 10000);
              break;

            case 'year':
              if (is_int($value)) {
                $this->jsonSource->$key = $value;
              }
              else {
                $this->jsonSource->$key = (int) $value;
                drupal_set_message(t('@key <i>@value</i> must be an integer.', [
                  '@key' => $key,
                  '@value' => $value,
                ]), 'warning', FALSE);
              }
              break;

            case 'identifiers':
              if (!empty($value) && is_array($value)) {
                $this->jsonSource->$key = array();
                foreach ($value as $key_identifier => $val_identifier) {
                  if (in_array($key_identifier, [
                    'arxiv',
                    'doi',
                    'isbn',
                    'issn',
                    'pmid',
                    'scopus',
                    'ssrn',
                  ])) {
                    if (!empty($val_identifier)) {
                      $this->jsonSource->$key[$key_identifier] = $this->setMaxString($key . ":" . $key_identifier, $val_identifier, 500);
                    }
                  }
                  else {
                    drupal_set_message(t("key @key for <i>@value</i> isn't recognized.", [
                      '@key' => $key_identifier,
                      '@value' => $val_identifier,
                    ]), 'warning', FALSE);
                  }
                }
              }
              break;

            case 'keywords':
              if (!empty($value) && is_array($value)) {
                $this->jsonSource->$key = array();
                foreach ($value as $val_keyword) {
                  $this->jsonSource->$key[] = $this->setMaxString($key, $val_keyword, 50);
                }
              }
              break;

            default:
              if (!empty($value)) {
                $this->jsonSource->$key = $value;
              }
              break;

          }
        }
      }
      else {
        drupal_set_message(t('mendeley_submit_document_core is required.'), 'warning', FALSE);
      }

      $documentsExtra = module_invoke_all('mendeley_submit_document_extra', $node);

      if (!empty($documentsExtra)) {
        foreach ($documentsExtra['mendeley_submit_document_extra'] as $key => $value) {
          switch ($key) {
            case 'revision':
            case 'issue':
            case 'publisher':
            case 'city':
            case 'institution':
            case 'series':
            case 'citation_key':
            case 'source_type':
            case 'language':
            case 'genre':
            case 'country':
            case 'series_editor':
            case 'code':
            case 'medium':
            case 'user_context':
            case 'department':
            case 'patent_owner':
            case 'patent_application_number':
            case 'patent_legal_status':
              $this->jsonSource->$key = $this->setMaxString($key, $value, 255);
              break;

            case 'pages':
            case 'short_title':
              $this->jsonSource->$key = $this->setMaxString($key, $value, 50);
              break;

            case 'volume':
            case 'chapter':
            case 'reprint_edition':
              $this->jsonSource->$key = $this->setMaxString($key, $value, 10);
              break;

            case 'websites':
              if (!empty($value) && is_array($value)) {
                $this->jsonSource->$key = array();
                foreach ($value as $val_keyword) {
                  $this->jsonSource->$key[] = $this->setMaxString($key, $val_keyword, 255);
                }
              }
              break;

            case 'edition':
              if (!empty($value) && is_array($value)) {
                $this->jsonSource->$key = array();
                foreach ($value as $val_keyword) {
                  $this->jsonSource->$key[] = $this->setMaxString($key, $val_keyword, 10);
                }
              }
              break;

            case 'tags':
              if (!empty($value) && is_array($value)) {
                $this->jsonSource->$key = array();
                foreach ($value as $val_keyword) {
                  $this->jsonSource->$key[] = $this->setMaxString($key, $val_keyword, 50);
                }
              }
              break;

            case 'month':
            case 'day':
              if (is_int($value)) {
                $this->jsonSource->$key = $value;
              }
              else {
                $this->jsonSource->$key = (int) $value;
                drupal_set_message(t('@key <i>@value</i> must be an integer.', [
                  '@key' => $key,
                  '@value' => $value,
                ]), 'warning', FALSE);
              }
              break;
          }
        }
      }
    }
  }

  /**
   * Get the node with language preferences.
   *
   * SC 03 Dec 2019 19:10:51 stefano.
   *
   * @param int $nid
   *   The node id.
   *
   * @return array
   *   The node and translated nodes.
   */
  public function getRealNode($nid) {
    global $language;

    // Search and set the main node language.
    //
    // SC 27 ago 2018 16:37:42 stefano.
    //
    $node_i18n = translation_node_get_translations($nid);

    $languages = language_list('enabled')[1];

    $default_lang = language_default('language');

    if (array_key_exists($default_lang, $node_i18n) && $node_i18n[$default_lang]->status) {
      $node = node_load($node_i18n[$default_lang]->nid);
    }
    else {
      drupal_set_message(t("The preferred language is #preflang, but at the moment this node isn't translated into #preflang. <strong>We use however this node as #lang for the main article.</strong>", ['#lang' => $language->name, '#preflang' => $languages[$default_lang]->name]), 'warning', FALSE);
      $node = node_load($nid);
    }

    return [$node, $node_i18n];
  }

  /**
   * Max length.
   *
   * Check the string max length and cuts it with message if exceed.
   *
   * SC 11 Dec 2019 17:35:01 stefano.
   *
   * @param string $key
   *   The item name.
   * @param string $value
   *   The item value.
   * @param int $max
   *   The item max length.
   *
   * @return string
   *   The adapted value.
   */
  private function setMaxString($key, $value, $max) {
    if (strlen(_medra_crossref_submit_escape_xml($value)) > $max) {
      drupal_set_message(t('@key <i>@value</i> is more than @count chars.', [
        '@key' => $key,
        '@count' => $max,
        '@value' => $value,
      ]), 'warning', FALSE);
      $value = substr($$value, 0, $max);
    }
    return $value;
  }

  /**
   * Node data for json.
   *
   * SC 09 Jan 2020 11:06:45 stefano.
   *
   * @return string
   *   The json data.
   */
  public function getJsonSource() {
    return $this->jsonSource;
  }

  /**
   * The json encoded data.
   *
   * SC 09 Jan 2020 11:51:47 stefano.
   *
   * @return string
   *   The json.
   */
  public function getJsonEncoded() {
    return json_encode($this->jsonSource);
  }

}

/**
 * Documents.
 *
 * SC 26 Nov 2019 18:34:05 stefano.
 */
class MendeleySubmitDocuments extends MendeleySubmit {

  /**
   * The json encoded data.
   *
   * @var string
   *
   * SC 09 Jan 2020 11:44:00 stefano.
   */
  private $jsonString;

  /**
   * Creating a document from metadata.
   *
   * SC 28 Nov 2019 11:54:54 stefano.
   */
  public function creating() {

    curl_setopt($this->ch, CURLOPT_URL, $this->endpoint . 'documents');
    curl_setopt($this->ch, CURLOPT_POST, TRUE);
    curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->jsonString);

    $this->addHeader('Accept: application/vnd.mendeley-document.1+json');
    $this->addHeader('Content-Type: application/vnd.mendeley-document.1+json');
  }

  /**
   * Set json encoded data.
   *
   * SC 09 Jan 2020 11:47:30 stefano.
   *
   * @param string $json
   *   The json data.
   */
  public function setJsonString($json) {
    $this->jsonString = $json;
  }

}

/**
 * Persons object.
 *
 * SC 26 Nov 2019 18:38:37 stefano.
 */
class MendeleySubmitPerson extends MendeleySubmitDocuments {

  /**
   * Last name of the person.
   *
   * Max length 255 chars.
   *
   * Required.
   *
   * @var string
   *
   * SC 27 Nov 2019 10:25:32 stefano.
   */
  private $lastName;

  /**
   * First name of the person.
   *
   * Max length 255 chars.
   *
   * @var string
   *
   * SC 27 Nov 2019 10:26:48 stefano.
   */
  private $firstName;

  /**
   * Check last name length.
   *
   * @var bool
   *
   * SC 03 Dec 2019 11:38:11 stefano.
   */
  private $lastNameCutted;

  /**
   * Check first name length.
   *
   * @var bool
   *
   * SC 03 Dec 2019 11:38:11 stefano.
   */
  private $firstNameCutted;

  /**
   * Persons.
   *
   * SC 28 Nov 2019 18:20:38 stefano.
   *
   * @param string $lastname
   *   The last name.
   * @param string $firstname
   *   The first name.
   */
  public function __construct($lastname, $firstname = '') {
    $this->lastNameCutted = FALSE;
    $this->firstNameCutted = FALSE;

    if (!empty($lastname)) {
      $this->lastName = substr($lastname, 0, 255);

      if ($this->lastName != $lastname) {
        $this->lastNameCutted = TRUE;
      }

      if (!empty($firstname)) {
        $this->firstName = substr($firstname, 0, 255);

        if ($this->firstName != $firstname) {
          $this->firstNameCutted = TRUE;
        }
      }
    }
  }

  /**
   * Author.
   *
   * SC 28 Nov 2019 18:41:09 stefano.
   */
  public function addAuthor() {
    if (empty($this->firstName)) {
      $this->authors[] = [
        'last_name' => $this->lastName,
      ];
    }
    else {
      $this->authors[] = [
        'first_name' => $this->firstName,
        'last_name' => $this->lastName,
      ];
    }

    if ($this->firstNameCutted) {
      $this->drupalMessageFirstName('authors');
    }
    if ($this->lastNameCutted) {
      $this->drupalMessageLastName('authors');
    }
  }

  /**
   * Editor.
   *
   * SC 28 Nov 2019 18:41:09 stefano.
   */
  public function addEditor() {
    if (empty($this->firstName)) {
      $this->editors[] = [
        'last_name' => $this->lastName,
      ];
    }
    else {
      $this->editors[] = [
        'first_name' => $this->firstName,
        'last_name' => $this->lastName,
      ];
    }

    if ($this->firstNameCutted) {
      $this->drupalMessageFirstName('editors');
    }
    if ($this->lastNameCutted) {
      $this->drupalMessageLastName('editors');
    }
  }

  /**
   * Translator.
   *
   * SC 28 Nov 2019 18:41:09 stefano.
   */
  public function addTranslator() {
    if (empty($this->firstName)) {
      $this->translators[] = [
        'last_name' => $this->lastName,
      ];
    }
    else {
      $this->translators[] = [
        'first_name' => $this->firstName,
        'last_name' => $this->lastName,
      ];
    }

    if ($this->firstNameCutted) {
      $this->drupalMessageFirstName('translators');
    }
    if ($this->lastNameCutted) {
      $this->drupalMessageLastName('translators');
    }
  }

  /**
   * Implements dupal message.
   *
   * SC 03 Dec 2019 11:51:03 stefano.
   *
   * @param string $type
   *   The type of person.
   */
  private function drupalMessageFirstName($type) {
    drupal_set_message(t('<i>!type</i> first name is too long and is cut.', ['!type' => $type]), 'warning');
  }

  /**
   * Implements dupal message.
   *
   * SC 03 Dec 2019 11:51:03 stefano.
   *
   * @param string $type
   *   The type of person.
   */
  private function drupalMessageLastName($type) {
    drupal_set_message(t('<i>!type</i> last name is too long and is cut.', ['!type' => $type]), 'warning');
  }

}
