<?php

/**
 * @file
 * Web service for Mendeley.
 *
 * Date: 13 Nov 2019 16:49:36
 * File: mendeley_submit.admin.inc
 * Author: stefano.
 */

/**
 * Implements API Form.
 *
 * SC 14 Nov 2019 12:51:47 stefano.
 */
function _mendeley_submit_admin_settings($form, &$form_state) {

  $form['mendeley_submit_credentials'] = [
    '#type' => 'fieldset',
    '#title' => t('Mendeley credentials'),
    '#description' => t('The user will be prompted to sign in to Mendeley with their Elsevier credentials if necessary. If they do not have an Elsevier account then they can register for one as part of this flow.'),
    '#weight' => -100,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['mendeley_submit_credentials']['mendeley_submit_username'] = [
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Username'),
    '#default_value' => variable_get('mendeley_submit_username', ''),
    '#weight' => 1,
    '#required' => TRUE,
  ];

  $form['mendeley_submit_credentials']['mendeley_submit_password'] = [
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Password'),
    '#default_value' => variable_get('mendeley_submit_password', ''),
    '#weight' => 1,
    '#required' => TRUE,
  ];

  $form['mendeley_submit_credentials']['mendeley_submit_secret'] = [
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Application secret'),
    '#description' => t('The value for the application secret you received when registering your application. If absolutely necessary, you can reset the secret for your application using the <i>My Applications</i> page in the Mendeley Developer Portal. <stron>Important</strong>: you must store the application secret value securely and must not expose the value to any users.'),
    '#default_value' => variable_get('mendeley_submit_secret', ''),
    '#weight' => 1,
    '#required' => TRUE,
  ];

  $form['mendeley_submit_clientid'] = [
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Client ID'),
    '#description' => t('The value for application ID you received when registering your application. You can check the ID of your application using the <i>My Applications</i> page in the Mendeley Developer Portal.'),
    '#default_value' => variable_get('mendeley_submit_clientid', ''),
    '#weight' => 1,
    '#required' => TRUE,
  ];

  $form['mendeley_submit_redirecturl'] = [
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Redirect url'),
    '#description' => t('The URL value for Redirection URL you set when registering your application. You can check or change the redirection URL of your application using the <i>My Applications</i> page in the Mendeley Developer Portal.'),
    '#default_value' => variable_get('mendeley_submit_redirecturl', ''),
    '#weight' => 2,
    '#required' => TRUE,
    // This variable is managed by oauth2_client module.
    '#access' => FALSE,
  ];

  $form['mendeley_submit_responsetype'] = [
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Response type'),
    '#description' => t('The value <i>must</i> be <strong>code</strong> indicating the code authorization grant type is required and an authorization code is desired in the response.'),
    '#default_value' => variable_get('mendeley_submit_responsetype', 'code'),
    '#weight' => 3,
    '#required' => TRUE,
    '#disabled' => TRUE,
    '#access' => FALSE,
  ];

  $form['mendeley_submit_scope'] = [
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Scope'),
    '#description' => t('The value <i>must</i> be <strong>all</strong> permitting the manipulation of all API resources.'),
    '#default_value' => variable_get('mendeley_submit_scope', 'all'),
    '#weight' => 4,
    '#required' => TRUE,
    '#disabled' => TRUE,
    '#access' => FALSE,
  ];

  $form['mendeley_submit_endpoint'] = [
    '#type' => 'select',
    '#options' => [
      'sandbox' => t('SANDBOX (@url)', ['@url' => MENDELEY_SUBMIT_ENDPOINT_DEV]),
      'production' => t('Production (@url)', ['@url' => MENDELEY_SUBMIT_ENDPOINT]),
    ],
    '#title' => t('Endpoint'),
    '#description' => t('The endpoint of web service.<strong>If "sandbox" please fill also the sandbox fields below.</strong>'),
    '#default_value' => variable_get('mendeley_submit_endpoint', ''),
    '#required' => TRUE,
  ];

  $form['mendeley_submit_sandbox'] = [
    '#type' => 'fieldset',
    '#title' => t('Sandbox'),
    '#description' => t('All beta endpoints require an additional <a href"@dev_token">Development Token</a>. These tokens live for a period of 90 days. This is to allow for reasonable amount of testing by clients of a beta endpoint.', ['@dev_token' => url("https://development-tokens.mendeley.com/")]),
    '#weight' => 100,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['mendeley_submit_sandbox']['#element_validate'][] = '_mendeley_submit_sandbox_validate';

  $form['mendeley_submit_sandbox']['mendeley_submit_token_s'] = [
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 256,
    '#title' => t('Dev token'),
    '#description' => t('The access token is displayed in the <i>oAuth Info</i> panel of a successfully authorized page. Copy the long sequence of characters, that form the access token, to use in the web service.'),
    '#default_value' => variable_get('mendeley_submit_token_s', ''),
    '#weight' => 1,
    '#required' => FALSE,
  ];

  // Node.
  //
  // SC 20 Dec 2019 12:18:41 stefano.
  //
  $form['mendeley_submit_node'] = [
    '#type' => 'fieldset',
    '#title' => t('Node'),
    '#description' => t('Options related to node.'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];

  $form['mendeley_submit_node']['mendeley_submit_ntype'] = [
    '#type' => 'select',
    '#title' => t('Content type'),
    '#options' => node_type_get_names(),
    '#description' => t('The manuscript content type ready for mEDRA/Crossref DOI.'),
    '#default_value' => variable_get('mendeley_submit_ntype', []),
    '#multiple' => TRUE,
    '#required' => TRUE,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 99,
  ];

  return $form;
}

/**
 * Implements form API element valildate.
 *
 * SC 27 Sep 2019 16:04:11 stefano.
 */
function _mendeley_submit_sandbox_validate($element, &$form_state, $form) {
  if ($form_state['values']['mendeley_submit_endpoint'] == 'sandbox') {
    if (empty($form_state['values']['mendeley_submit_token_s'])) {
      form_error($element, t('With endpoint "sandbox" you must fill all "Sandbox" fields.'));
    }
  }
}

/**
 * Implements form API submit.
 *
 * SC 27 Nov 2019 18:01:43 stefano.
 */
function _mendeley_submit_admin_settings_submit($form, &$form_state) {
  variable_set('mendeley_submit_username', $form_state['values']['mendeley_submit_username']);
  variable_set('mendeley_submit_password', $form_state['values']['mendeley_submit_password']);
  variable_set('mendeley_submit_secret', $form_state['values']['mendeley_submit_secret']);
  variable_set('mendeley_submit_clientid', $form_state['values']['mendeley_submit_clientid']);
  variable_set('mendeley_submit_redirecturl', (isset($form_state['values']['mendeley_submit_redirecturl']) ? $form_state['values']['mendeley_submit_redirecturl'] : ''));
  variable_set('mendeley_submit_responsetype', (isset($form_state['values']['mendeley_submit_responsetype']) ? $form_state['values']['mendeley_submit_responsetype'] : 'code'));
  variable_set('mendeley_submit_scope', (isset($form_state['values']['mendeley_submit_scope']) ? $form_state['values']['mendeley_submit_scope'] : 'all'));
  variable_set('mendeley_submit_endpoint', $form_state['values']['mendeley_submit_endpoint']);
  variable_set('mendeley_submit_token_s', $form_state['values']['mendeley_submit_token_s']);
  variable_set('mendeley_submit_ntype', $form_state['values']['mendeley_submit_ntype']);
}
